<?php

// on récupère les nom prenom age 
session_start();
//à mettre au début pour elver les retours d'erreurs à chaque fois
if (isset($_SESSION['messages'])){
    unset ($_SESSION['messages']);//supprime les messages d'erreurs si ils n'ont plus lieu d'être
}

$nom = filter_input(INPUT_POST, 'nom', FILTER_VALIDATE_REGEXP, // récupère false si ce qu'on entre ne correspond pas au filtre. NUll si le formulaire a été modifier ou si rien
        ['options' => ['regexp' => '/^[a-z]{1,40}$/']]);
$prenom = filter_input(INPUT_POST, 'prenom', FILTER_VALIDATE_REGEXP, 
        ['options' => ['regexp' => '/^[a-z]{1,40}$/']]);
$color = filter_input(INPUT_POST, 'color', FILTER_VALIDATE_REGEXP, 
        ['options' => ['regexp' => '/^[a-z]{1,40}$/']]);
//$sexe = filter_input(INPUT_POST, 'sexe', FILTER_VALIDATE_REGEXP, // pas le même filtre pour l'age
//        ['options' => ['regexp' => '/(homme)|(femme)/']]);

//echo "$nom $prenom $age";//vérifie si ça fonctionne 
$message = []; // ici on indique si la saisie est correcte ou non 
if (!$nom) { //ici c'est not true = donc si false je rentre pas soit si nom est rempli est vrai si pas rempli false = si rien dans nom on n'y va pas = si fals ou nul on entre car not false est vrai 
    $message[] = "Le nom est incorrect.";
}
if (!$prenom) {
    $message[] = "Le prenom est incorrect.";
}
if (!$color) {
    $message[] = "c'est quoi cette couleur.";
}
//if(!$sexe){
//    $message[] = "Le genre est incorrect.";
//}

if (empty($message)) {
    $user_chek = ['nom' => $nom, 'prenom' => $prenom, 'color' => $color]; //on met dans un tab user 
    $_SESSION ['user'] = $user_chek; // vu que nom prenom et age son dans un tab user tout peut être 
    ////mis dans $_SESSION et donc plus simple dans la lisibilité sinon on aurait fait $_SESSION = ['nom']; etc 
    //côté serveur ce qu'il y a ds $_SESSION ['user'] est sauvegarder
    header('location: return.php');
}else {
    $_SESSION['messages'] = $message;
    header('location: index.php');
}



